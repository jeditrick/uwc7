<?php

require "vendor/autoload.php";
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response;

ini_set('memory_limit', '200M');
$router = new League\Route\RouteCollection;


$router->get('/', function (Request $request, Response $response) {
    include 'src/views/home.php';
    return $response;
});

$router->get('/result', '\Kolya\MainController::index');
$router->get('/get_xml/{filename}', function (Request $request, Response $response, array $args) {
    $filename = $args['filename'];

    $filename = 'xml/' . $filename . '.xml';
    header('Pragma: public');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private', false);
    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment; filename="' . basename($filename) . '";');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: ' . filesize($filename));
    readfile($filename);

    return $response;
});


$dispatcher = $router->getDispatcher();
$request = Request::createFromGlobals();
$response = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());
$response->send();
