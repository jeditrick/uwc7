<?php
namespace Kolya;

use League\Route\Http\Exception;

class MainModel
{
    public $_domainUrl;
    public $urlList = [];

    public function __construct($url)
    {
        if ($this->testDomain($url)) {
            $this->_domainUrl = $url;
        } else {
            throw new Exception('Failed to init model');
        }
    }


    public function testDomain($url)
    {
        $pattern = '/^http(s?):\/\/([\wа-яА-Я.ёЁ-]+)\.([а-яА-Яa-zA-Z\.]{2,})?/';

        return preg_match($pattern, $url);
    }


    public function validateNestedUrl($url, $urlList, $badUrlList, $domainUrl)
    {
        if (strpos($url, $domainUrl) === false) {
            return !strpos($url, $this->_domainUrl) && !strpos($url, '#') && !strpos(
                $url,
                '.exe'
            ) && $url != null && !array_key_exists($url, $urlList) && !array_key_exists($url, $badUrlList) && (strpos(
                    $url,
                    'www.'
                ) === false) && (strpos($url, 'http://') === false) && (strpos($url, 'https://') === false) && (strpos(
                    $url,
                    'file://'
                ) === false) && (strpos($url, 'ftp://') === false) && (strpos($url, 'javascript') === false);
        }

        return !strpos($url, $this->_domainUrl) && !strpos($url, '#') && !strpos(
            $url,
            '.exe'
        ) && $url != null && !array_key_exists($url, $urlList) && !array_key_exists($url, $badUrlList);
    }

    public function filterNestedUrl($url)
    {
        $clear_domain = "";
        if (strpos($this->_domainUrl, 'http://') !== false) {
            $clear_domain = str_replace(array('http://'), '', $this->_domainUrl);
        } elseif (strpos($this->_domainUrl, 'https://') !== false) {
            $clear_domain = str_replace(array('https://'), '', $this->_domainUrl);
        }

        if ($clear_domain != "") {
            if (strpos($url, $clear_domain) !== false) {
                $url = substr($url, (strpos($url, $clear_domain) + strlen($clear_domain)));
            }
            $url = ($url[0] != '/') ? '/' . $url : $url;
        }

        return $url;
    }

    public function filterBadUrl($url)
    {
        $url = str_replace($this->_domainUrl, '', $url);

        $url = preg_replace('/\/.+/', '', $url);
        $url = ($url[0] != '/') ? '/' . $url : $url;

        return $url;
    }


    public function validateHeader($header)
    {
        return strpos($header, 'ext/html');
    }

} 