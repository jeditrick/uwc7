<style>

    body *{
        font-family: "Arial Black", Arial, Tahoma, sans-serif,serif;
    }

    .main{
        text-align: center;
        margin-top: 12%;
    }
    input{
        margin: 15px;

    }
    #url{
        width: 300px;
        padding: 5px;
        font-size: 16px;
        border: 0;
        border-bottom: 2px solid black;
    }
    #url:focus{
        outline: 0;
    }
    [type="submit"]{
        outline: 0;
        width: 150px;
        height: 50px;
        border: 0;
        box-shadow: 0  3px 3px rgba(0,0,0,0.3);
    }
    [type="submit"]:hover{

        color: gray;
    }

</style>

<div class="main">
<form>
    <label for="url">Enter url : </label>
    <br>
    <input type="text" id="url" name="url">
    <br>
    <input type="submit" value="Send">
</form>
<a id="result">

</div>

</div>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script>
    $('[type="submit"]').click(function(e){
        e.preventDefault();
        var url = $('#url').val();
        $.get(
            "result",
            {url:url},
            function(response){
                console.log(response);
                $('#result').text('');
                $('#result').append(response);
                $('[type="submit"]').removeAttr('disabled');
            }
        );

        $('#result').text('Please wait...');
        $('[type="submit"]').attr('disabled','true');

    });
</script>