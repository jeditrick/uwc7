<?php
namespace Kolya;

use GuzzleHttp\Event\BeforeEvent,
    GuzzleHttp\Event\CompleteEvent,
    GuzzleHttp\Exception\ServerException,
    GuzzleHttp\Client,
    GuzzleHttp\Pool,
    GuzzleHttp\Exception\ClientException,
    Symfony\Component\DomCrawler\Crawler,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\Response,
    Katzgrau\KLogger\Logger,
    League\Route\Http\Exception;

class MainController
{
    private $_urlList = [];
    private $_buffUrlList = [];
    private $_badUrlList = [];
    private $_domainUrl;
    private $_logger;
    private $_client;
    private $_filename;
    private $_xml;

    public function __construct()
    {
        $this->_logger = new Logger('log/');
        $this->_client = new Client();

        try {
            $this->Model = new MainModel($_GET['url']);
            $this->_domainUrl = trim($this->Model->_domainUrl, '/');
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }


    public function index(Request $request, Response $response)
    {
        $this->_filename = str_replace(array('http://', 'https://'), '', $this->_domainUrl);
        $this->_filename .= '_' . Date('Y-m-d_G:i');

        $this->_xml = new MyXmlWriter($this->_filename);

        $this->_logger->info("--------------/ Start Log (" . date('Y-m-d G:i') . ") /--------------");
        $this->parseURL(array($this->Model->_domainUrl));
        $this->_logger->info("--------------/ End Log (" . date('Y-m-d G:i') . ") /--------------");
        $this->_xml->getXML();
        if (count($this->_urlList) > 0) {
            echo $this->getDownloadUrl($this->_xml->getFilePath());
        } else {
            echo 'bad URL';
            $request->get('/');
        }

        return $response;
    }

    function parseURL($url, $deep = 0)
    {
        $this->_buffUrlList = [];
        $localUrlScope = $url;


        Pool::send(
            $this->_client,
            $this->urlGenerator($localUrlScope),
            [
                'complete' => function (CompleteEvent $response) use ($deep) {
                    $crawler = new Crawler("<<<'HTML'" . $response->getResponse()->getBody() . "HTML;");
                    $crawler->filter('a')->each(
                        function (Crawler $node) use ($deep) {
                            $url = $this->Model->filterNestedUrl($node->attr('href'));
                            if ($this->Model->validateNestedUrl($url, $this->_urlList, $this->_badUrlList,
                                $this->_domainUrl, $this->_domainUrl)
                            ) {
                                $resultUrl = $this->_domainUrl . $url;
                                try {
                                    $req = $this->_client->get($resultUrl);
                                    $header = $req->getHeader('Content-Type');
                                    $responseUrl = $req->getEffectiveUrl();
                                    if ($this->Model->validateHeader($header)) {
                                        $this->_buffUrlList[$url] = $responseUrl;
                                        $this->_urlList[$url] = $responseUrl;
                                        $this->_xml->add($responseUrl);
                                        $this->_logger->info("  deeep[$deep] buff_count[" . count($this->_buffUrlList) . "] main_count[" . count($this->_urlList) . "] $responseUrl");
                                    } else {
                                        $this->_badUrlList[$url] = '';
                                    }
                                } catch (ClientException $e) {
                                    $this->_badUrlList[$url] = $resultUrl;
                                    $this->_logger->error("  deeep[$deep] buff_count[" . count($this->_buffUrlList) . "]  \n" . $e->getMessage()
                                    );
                                } catch (ServerException $e) {
                                    $this->_badUrlList[$url] = $resultUrl;
                                    $this->_logger->error(
                                        "  deeep[$deep] buff_count[" . count(
                                            $this->_buffUrlList
                                        ) . "]  \n" . $e->getMessage()
                                    );
                                }
                            } else {
                                $this->_badUrlList[$url] = '';
                            }
                        }
                    );

                },
                'before' => function (BeforeEvent $e) {
                    $e->getRequest()->setHeader('User-Agent',
                        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36');
                }
            ]
        );
        if ($deep++ < 4) {
            $this->parseURL($this->_buffUrlList, $deep);
        }

    }

    public function urlGenerator($urlArr)
    {
        $requests = [];
        foreach ($urlArr as $url) {
            array_push($requests, $this->_client->createRequest('GET', $url));
        }

        return $requests;
    }

    public function getUrlCount(Request $request, Response $response)
    {
        echo json_encode(
            array(
                'count' => count($this->_urlList)
            )
        );

        return $response;
    }

    public function getDownloadUrl()
    {
        return "<a href='get_xml/" . $this->_filename . "'>Get Sitemap</a>";
    }

}