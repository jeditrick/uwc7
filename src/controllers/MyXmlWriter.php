<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 26.03.15
 * Time: 21:18
 */

namespace Kolya;

use \XMLWriter as XMLWriter;

class MyXmlWriter extends XMLWriter
{
    private $filename;
    private $filepath;

    public function __construct($filename)
    {

        $this->openUri('xml/' . $filename . '.xml');
        $this->setIndent(true);
        $this->startDocument();
        $this->startElement('urlset');
        $this->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

        echo $this->outputMemory();
    }

    public function add($url)
    {
        $this->startElement('url');
        $this->writeElement('loc', $url);
        $this->endElement();
    }


    public function getXML()
    {
        $this->endElement();
        $this->endDocument();
        $this->flush();
    }

    public function getFilePath()
    {
        return $this->filepath;
    }

} 